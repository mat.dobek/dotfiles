" ====================
"  PLUGINS
" ====================

call plug#begin('~/.vim/plugged')

Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
Plug 'junegunn/fzf.vim'
Plug 'scrooloose/nerdtree'
Plug 'tpope/vim-fugitive'
Plug 'tpope/vim-dispatch'

" Syntax
Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
Plug 'rizzatti/dash.vim'
Plug 'scrooloose/nerdcommenter'

" Ruby
Plug 'tpope/vim-rails'

" Rust
Plug 'rust-lang/rust.vim'
Plug 'racer-rust/vim-racer'

" Markdown and Writing
Plug 'junegunn/goyo.vim'
Plug 'godlygeek/tabular'
Plug 'plasticboy/vim-markdown'
Plug 'rhysd/vim-grammarous'
Plug 'beloglazov/vim-online-thesaurus'

call plug#end()



" ====================
"  GLOBAL
" ====================

set nocompatible                                        " turn off compatibility with Vi

filetype indent on                                      " custom filetype indentation rules

set tabstop=2                                           " size (in spaces) of tab
set shiftwidth=2                                        " affects pressing '<<' , '>>' , =='
set expandtab                                           " expand tab to spaces
set autoindent                                          " copy indentation from previous line, and apply to next one

set hlsearch                                            " highlight searches
set incsearch                                           " start searching when you type
set ignorecase                                          " ignore case when searching

set laststatus=2                                        " last window will have a status line always visible
set statusline=File:\ %f\ Line:\ %l                     " define how status line should look like

set relativenumber                                      " display relative number
set noswapfile                                          " do not use swapfiles, as files are modified by one user at time

set undodir=~/.vim/undodir                              " specify where to keep history
set undofile                                            " keep history after exiting file

syntax on                                               " enable syntax highlighting
colorscheme nofrils-dark                                " use given colorscheme

let g:netrw_localrmdir='rm -r'                          " allow netrw to remove non-empty local directories
let g:netrw_dirhistmax=0                                " prevent netrw from saving history, and no .netrwhist files



" ====================
"  FUNCTIONS
" ====================

function! TrimWhiteSpace()
  %s/\s\+$//e
endfunction

autocmd BufWritePre * :call TrimWhiteSpace()            " trim white spaces on save



" ====================
"  OTHER SETTINGS
" ====================

"
" DEFAUTS
"

noremap <Leader>rr :Dispatch %<cr>

"
" DEOPLATE
"

" deoplete options
let g:deoplete#enable_at_startup = 1
let g:deoplete#enable_smart_case = 1

" disable autocomplete by default
" let b:deoplete_disable_auto_complete=1
" let g:deoplete_disable_auto_complete=1

" Disable the candidates in Comment/String syntaxes.
call deoplete#custom#source('_',
            \ 'disabled_syntaxes', ['Comment', 'String'])

" set sources
"let g:deoplete#sources = {}
"let g:deoplete#sources.rust = ['LanguageClient']
"let g:deoplete#sources.vim = ['vim']

"
" MARKDOWN
"

au BufNewFile,BufReadPost *.{md,mdown,mkd,mkdn,markdown,mdwn} set filetype=markdown

au FileType markdown setlocal spell           " Spellchecking for Markdown
au FileType markdown setlocal spelllang=en_gb " Set region
au FileType markdown setlocal linebreak       " Avoid wrap breaking words
au FileType markdown setlocal nolist          " Make sure linebreak work as expected
au FileType markdown setlocal showbreak=↳\    " Know where we're
au FileType markdown setlocal textwidth=0     " Remove text width limit
au FileType markdown setlocal nornu           " Do not display relative line number
"au FileType markdown Goyo                     " Automatically turn on Goyo when opening markdown

"
" plasticboy/vim-markdown
"

let g:vim_markdown_folding_style_pythonic = 1
let g:vim_markdown_folding_level = 1        " Set header folding level
let g:vim_markdown_new_list_item_indent = 2 " Set the number of spaces of indent
let g:vim_markdown_toc_autofit = 1          " Enable TOC window auto-fit
let g:vim_markdown_conceal = 0              " Do not conceal [link text](link url) as just link text
let g:vim_markdown_frontmatter = 1          " Highlight YAML front matter as used by Jekyll or Hugo.

"
" RUST
"

let g:rustfmt_autosave = 1                              " format rust syntax, on save .rs files

autocmd FileType rust noremap <leader>ri :terminal cargo run<cr>
autocmd FileType rust noremap <leader>rr :Dispatch cargo run<cr>
autocmd FileType rust noremap <leader>rt :Dispatch rustc % && ./test<cr>

"
" rust-lang/rust.vim
"

" do not add comments automatically
autocmd FileType * setlocal formatoptions-=c formatoptions-=r formatoptions-=o

"
" racer-rust/vim-racer
"

" Also it's worth turning on 'hidden' mode for buffers otherwise you need to save the current buffer every time you do a goto-definition
set hidden
let g:racer_cmd = "/Users/matDobek/.cargo/bin/racer"
let g:racer_experimental_completer = 1

au FileType rust nmap <leader>drdoc <Plug>(rust-doc)
au FileType rust nmap <leader>drdef <Plug>(rust-def)
"au FileType rust nmap <leader>drs <Plug>(rust-def-split)
"au FileType rust nmap <leader>drx <Plug>(rust-def-vertical)

"
" RUBY
"

" run ruby for current file
autocmd FileType ruby noremap <Leader>rr :Dispatch ruby % <CR>

" run minitest test for current file
autocmd FileType ruby noremap <Leader>rtm :Dispatch bin/minitest % <CR>

" run spec test for current file
autocmd FileType ruby noremap <Leader>rts :Dispatch rspec % <CR>

" create alternate file
autocmd FileType ruby noremap <Leader>ra :! /Users/matDobek/Dropbox/projects/done/rails_alternate_file/main %:p minitest <CR>



" ====================
"  MAPPINGS
" ====================

let mapleader = " "

" return to normal mode in terminal via esc
tnoremap <Esc> <C-\><C-n>

" copy to system clipbord
noremap <leader>cc "*yy

" cycle through buffers
nnoremap <S-k> :bn<cr>
nnoremap <S-j> :bp<cr>

" quit Hlsearch
noremap qq :noh<cr>

" close buffer, without breaking the split
noremap <Leader>bd :b#<bar>bd#<CR>

" split buffer vertically
noremap <Leader>v :vsplit<CR>

" open NERDTree
noremap <Leader>p :NERDTree<cr>

" resize by arrows in normal mode
nnoremap <left> :vertical resize +5<cr>
nnoremap <right> :vertical resize -5<cr>

" Create an alternate file
nnoremap <Leader>A :!/Users/matDobek/Dropbox/projects/done/rails_alternate_file/main %:p minitest<cr>

"
" SEARCH / FZF
"
" https://github.com/junegunn/fzf.vim#commands

" search from all files in directory
noremap <leader>sf :Files ./<cr>

" search for keywords
noremap <leader>sk :Ag


"
" SPELL CHECKING
"
" add new word to dictionary
nnoremap <Leader>sa zg

" delete word from dictionary
nnoremap <Leader>sd zw

" correct to the first suggested word
nnoremap <Leader>sc 1z=

" list of correct words
nnoremap <Leader>sl z=

" next mispelled world
nnoremap <Leader>sn ]s

" previous mispelled world
nnoremap <Leader>sp [s

"
" FOLDING
"

" fold / unfold
nnoremap <tab> za

" close all folds
nnoremap <leader>fc zM

" open all folds
nnoremap <leader>fo zR

"
" DOCUMENTATION
"

" open documentation
nmap <leader>dd :Dash

" regenerate ctags
nmap <leader>dr :! ctags -R --languages=Rust,ruby --exclude=.git --exclude=log .<cr>

" ctag - follow the method
nmap <leader>df <C-]>

" ctag - take me back
nmap <leader>db <C-t>

"
" TABULARIZE
"

nmap <leader>t= :Tabularize /=<CR>
vmap <leader>t= :Tabularize /=<CR>
nmap <leader>t: :Tabularize /:\zs<CR>
vmap <leader>t: :Tabularize /:\zs<CR>

"
" nerdcommenter
"

vmap <leader>/ <Plug>NERDCommenterToggle
nmap <leader>/ <Plug>NERDCommenterToggle
