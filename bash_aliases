# Misc ----------------------------------------------------
alias ..='cd ..'
alias dot='j dotfiles'
alias wiki='j wiki && vim .'
alias vimrc='vim ~/.config/nvim/init.vim'

alias reload='source ~/.bash_profile'
alias top='top -o cpu'
alias vim='nvim'

alias dua='du -ms ./* | sort -n | tail -n 20'
alias dur='du -m | sort -n | tail -n 20'

# Rails ----------------------------------------------------
alias rs="rails s"
alias rc="rails c"
alias be="bundle exec"
alias bi="bundle install"
alias bu="bundle update"

# Git ----------------------------------------------------
alias gb='git branch'
alias gbd='git branch | grep -v "\(master\|production\|beta\)" | xargs git branch -d'
alias gbD='git branch | grep -v "\(master\|production\|beta\)" | xargs git branch -D'
alias gcm='git commit -m'
alias gco='git checkout'
alias gcof='git checkout $(git branch | fzf)'
alias gd='git diff'
alias gdc='git diff --cached'
alias gs='git status'
alias gpp='git pull; git push'
alias gpr='open "https://github.com/matDobek/`basename $(git rev-parse --show-toplevel)`/compare/master...`git rev-parse --abbrev-ref HEAD`?expand=1"'
alias gprn='open "https://github.com/netguru/`basename $(git rev-parse --show-toplevel)`/compare/master...`git rev-parse --abbrev-ref HEAD`?expand=1"'

alias gme='open "https://github.com/matDobek/"'

alias gitignore_rust="curl -L -s https://www.gitignore.io/api/rust > .gitignore && printf 'tags\n.DS_Store' >> .gitignore"

# Others ----------------------------------------------------
alias tmux="TERM=screen-256color-bce tmux"
