# --------------------
# Colors
# --------------------

COLOR_NO_COLOR="\033[0m"
COLOR_GREY="\033[38;5;240m"



# --------------------
# Aliases
# --------------------

source ~/.bash_aliases



# --------------------
# History
# --------------------

export HISTCONTROL=ignoredups
export HISTFILESIZE=3000
export HISTIGNORE="ls:cd:..:...:exit"



# --------------------
# Global
# --------------------

export EDITOR=nvim

# Input stuff
bind "set show-all-if-ambiguous On" # show list automatically, without double tab

# Add completion to source and .
complete -F _command source
complete -F _command .



# --------------------
# Prompts
# --------------------

prompt_symbol="❯"

git_branch() {
  git branch 2> /dev/null | grep "\*" | awk '{print ""$2""}'
}

git_dirty_flag() {
  git status 2> /dev/null | grep -c : | awk '{if ($1 > 0) print "+-"}'
}

prompt_func() {
    prompt="\n${COLOR_GREY}$ ${PWD}   $(git_branch)   $(git_dirty_flag) ${COLOR_NO_COLOR}"

    PS1="${prompt}\n${prompt_symbol} "
}

PROMPT_COMMAND=prompt_func



# --------------------
# Path
# --------------------

PATH=$PATH:/usr/local/bin:/opt/local/bin:/opt/local/sbin
PATH=$PATH:$HOME/.cargo/bin                                 # rust
PATH=$PATH:/usr/local/share/npm/bin                         # npm
PATH=$PATH:$HOME/.rvm/bin                                   # rvm
PATH=$PATH:/usr/local/heroku/bin                            # heroku
PATH=$PATH:$HOME/scripts                                    # my own scripts
export PATH=$PATH

export RUST_SRC_PATH="$(rustc --print sysroot)/lib/rustlib/src/rust/src"

# --------------------
# 3rd porty scripts
# --------------------

[ -f ~/.rvm/scripts/rvm ] && source ~/.rvm/scripts/rvm
[ -f ~/.fzf.bash ] && source ~/.fzf.bash
[ -f /usr/share/autojump/autojump.sh ] && source /usr/share/autojump/autojump.sh # this needs to be in the end
